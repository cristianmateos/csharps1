﻿using System;

namespace H4_BMI_if
{
    class Program
    {
        static void Main(string[] args)
        {
            double bmi;
            
            Console.WriteLine("Hoe veel weeg je in kg?");
            double OwnWeight = Convert.ToDouble(Console.ReadLine());
            
            Console.WriteLine("Hoe groot ben je in meter?");
            double OwnHeight = Convert.ToDouble(Console.ReadLine());

            // BMI calculation
            bmi = OwnWeight / (OwnHeight * OwnHeight);

            if (bmi < 18.5)
            {
                Console.WriteLine($"Je BMI bedraagt {bmi:F2}!");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Ondergewicht!!!");
            }
            else if (bmi > 18.5 && bmi < 25)
            {
                Console.WriteLine($"Je BMI bedraagt {bmi:F2}!");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Normaal gewicht");
            }
            else if (bmi > 25 && bmi < 30)
            {
                Console.WriteLine($"Je BMI bedraagt {bmi:F2}!");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Overgewicht");
            }
            else if (bmi > 30 && bmi < 40)
            {
                Console.WriteLine($"Je BMI bedraagt {bmi:F2}!");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Zwaarlijvig");
            }
            else {
                Console.WriteLine($"Je BMI bedraagt {bmi:F2}!");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine("Ernstige obesitas!!");
            }
            
            Console.ReadKey();
        }
    }
}
