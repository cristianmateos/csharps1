﻿using System;

namespace H4_schoenverkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            const int regularShoePrice = 20;
            const int discountedShoePrice = 10;
            
            Console.WriteLine("Vanaf welk aantal geldt de korting?");
            int discountTreshold = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Hoeveel paar schoenen wil je kopen?");
            int shoeOrder = Convert.ToInt32(Console.ReadLine());

            if (shoeOrder >= discountTreshold)
            {
                Console.WriteLine($"Je moet {shoeOrder * discountedShoePrice } euro betalen.");
            }
            else
            {
                Console.WriteLine($"Je moet {shoeOrder * regularShoePrice} euro betalen.");
            }
            Console.ReadKey();
        }
    }
}
