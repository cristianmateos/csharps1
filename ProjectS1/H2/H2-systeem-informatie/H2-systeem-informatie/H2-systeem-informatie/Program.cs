﻿using System;

namespace H2_systeem_informatie
{
    class Program
    {
        static void Main(string[] args)
        {
            //Retrieve computer info 
            
            bool is64bit = Environment.Is64BitOperatingSystem;
            string pcName = Environment.MachineName;
            int procCount = Environment.ProcessorCount;
            string userName = Environment.UserName;
            long memory = Environment.WorkingSet;




            Console.WriteLine($"Uw computer heeft een 64-bit besturingssysteem: {is64bit}");
            Console.WriteLine($"De naam van uw pc is: {pcName}");
            Console.WriteLine($"Uw pc heeft {procCount} processorkernen.");
            Console.WriteLine($"{userName} is uw gebruikersnaam.");
            Console.WriteLine($"Je gebruikt {(memory / 1024) / 1024} megabytes aan geheugen");

            Console.ReadKey();
        }
    }
}
