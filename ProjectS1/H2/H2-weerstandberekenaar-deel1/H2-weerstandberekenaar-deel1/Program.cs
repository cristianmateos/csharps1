﻿using System;

namespace H2_weerstandberekenaar_deel1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declare variables

            string ring1, ring2;
            int ring0;
            double ring3;

            Console.WriteLine("Geef de waarde(uitgedrukt in een getal van 0 tot 9) van de eerste ring: ");
            ring1 = Console.ReadLine();

            Console.WriteLine("Geef de waarde (uitgedrukt in een getal van 0 tot 9) van de tweede ring: ");
            ring2 = Console.ReadLine();

            Console.WriteLine("Geef de waarde (uitgedrukt in een getal van -2 tot 7) van de derde ring (exponent): ");
            ring3 = int.Parse(Console.ReadLine());
            ring3 = Math.Pow(10, ring3);

            ring0 = int.Parse(ring1 + ring2);

            Console.WriteLine($"Resultaat is {ring0 * ring3} Ohm, ofwel {ring0}x{ring3}.");

            Console.ReadKey();
        }
    }
}
