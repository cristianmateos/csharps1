﻿using System;

namespace H1_maaltafels
{
    class Program
    {
        static void Main(string[] args)
        {
            //Fixed Data variables

            int number1 = 411;

            //Clear; Calculation and wait for user to continue

            Console.Clear();
            Console.WriteLine($"1 x {number1} = " + (1 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"2 x {number1} = " + (2 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"3 x {number1} = " + (3 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"4 x {number1} = " + (4 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"5 x {number1} = " + (5 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"6 x {number1} = " + (6 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"7 x {number1} = " + (7 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"8 x {number1} = " + (8 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"9 x {number1} = " + (9 * number1));
            Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"10 x {number1} = " + (10 * number1));
            
            //Press key to exit

            Console.ReadKey();
        }
    }
}
