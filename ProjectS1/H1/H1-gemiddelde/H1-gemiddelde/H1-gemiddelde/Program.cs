﻿using System;

namespace H1_gemiddelde
{
    class Program
    {
        static void Main(string[] args)
        {
            //Data integers
            
            int number1 = 18;
            int number2 = 11;
            int number3 = 8;
            
            //Calculation integers

            Console.WriteLine("Het gemiddelde van de voorgedefineerde integers is: " + (number1 + number2 + number3) / 3);

            //Data floats
            
            float number4 = 18f;
            float number5 = 11f;
            float number6 = 8.0f;

            //Calculation floats

            Console.WriteLine("Het gemiddelde van de voorgedefineerde floats is: " + (number4 + number5 + number6) / 3);

            //Press key to exit

            Console.ReadKey();
        }
    }
}
