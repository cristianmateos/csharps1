﻿using System;

namespace H1_ruimte
{
    class Program
    {
        static void Main(string[] args)
        {
            //Actual weight  

            float weight = 100.0f;
            
            //Calculation

            Console.WriteLine($"Op Mercurius heb je een schijnbaar gewicht van {weight * 0.38f}.");
            Console.WriteLine($"Op Venus heb je een schijnbaar gewicht van {weight * 0.91f}.");
            Console.WriteLine($"Op Aarde heb je een schijnbaar gewicht van {weight * 1.0f}.");
            Console.WriteLine($"Op Mars heb je een schijnbaar gewicht van {weight * 0.38f}.");
            Console.WriteLine($"Op Jupiter heb je een schijnbaar gewicht van {weight * 2.34f}.");
            Console.WriteLine($"Op Saturnus heb je een schijnbaar gewicht van {weight * 1.06f}.");
            Console.WriteLine($"Op Uranus heb je een schijnbaar gewicht van {weight * 0.92f}.");
            Console.WriteLine($"Op Neptunus heb je een schijnbaar gewicht van {weight * 1.19f}.");
            Console.WriteLine($"Op Pluto heb je een schijnbaar gewicht van {weight * 0.06f}.");
            Console.ReadKey();
        }
    }
}
