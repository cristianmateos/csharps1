﻿using System;

namespace H1_verbruik_wagen
{
    class Program
    {
        static void Main(string[] args)
        {
            //Variables

            double tankBefore = 0d;
            double tankAfter = 0d;
            double kilometersBefore = 0d;
            double kilometersAfter = 0d;
            double consumption = 0d;
            
            //User interaction
            Console.WriteLine("Hoeveel liter zat er in tank voor de rit? ");
            tankBefore = double.Parse(Console.ReadLine());

            Console.WriteLine("Hoeveel liter zat er in tank na de rit? ");
            tankAfter = double.Parse(Console.ReadLine());

            Console.WriteLine("Wat was de kilometerstand voor de rit? ");
            kilometersBefore = double.Parse(Console.ReadLine());

            Console.WriteLine("Wat was de kilometerstand na de rit? ");
            kilometersAfter = double.Parse(Console.ReadLine());

            //Calculation
            consumption = 100 * (tankBefore - tankAfter) / (kilometersAfter - kilometersBefore);

            //Show consumption
            Console.WriteLine($"De auto verbruikt {consumption}!");

            //Wait for user to close
            Console.ReadKey();

        }
    }
}
