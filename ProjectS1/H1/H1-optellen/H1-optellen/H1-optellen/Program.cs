﻿using System;

namespace H1_optellen
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start variables

            int number1 = 0;
            int number2 = 0;
            int sum1 = 0;

            string input = string.Empty;

            //change color
            Console.ForegroundColor = ConsoleColor.Green;
            
            
            
            Console.WriteLine("Please enter the first number: ");

            //User Input1
            
            input = Console.ReadLine();
            number1 = int.Parse(input);


            Console.WriteLine("Please enter the second number: ");

            //User Input2

            input = Console.ReadLine();
            number2 = int.Parse(input);

            //Calculation

            sum1 = number1 + number2;

            //Solution

            Console.WriteLine($"De som is: {sum1}");

            //User action to exit

            Console.ReadKey();



        }
    }
}
