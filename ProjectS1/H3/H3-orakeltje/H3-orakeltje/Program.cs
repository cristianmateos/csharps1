﻿using System;

namespace H3_orakeltje
{
    class Program
    {
        static void Main(string[] args)
        {
            Random numbers = new Random();
            int yearsToLive = numbers.Next(5,126);
            Console.WriteLine($"Je hebt nog {yearsToLive} jaar te leven!");
            Console.ReadKey();
        }
    }
}
