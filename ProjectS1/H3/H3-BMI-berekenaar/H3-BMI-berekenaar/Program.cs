﻿using System;

namespace H4_BMI_if
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hoe veel weeg je in kg?");
            double OwnWeight = Convert.ToDouble(Console.ReadLine());
            
            Console.WriteLine("Hoe groot ben je in meter?");
            double OwnHeight = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"Je BMI bedraagt {OwnWeight / (OwnHeight * OwnHeight):F2}!");
            Console.ReadKey();

        }
    }
}
