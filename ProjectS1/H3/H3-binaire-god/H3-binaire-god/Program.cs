﻿using System;

namespace H3_binaire_god
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een getal in:");
            int numberInput = Convert.ToInt32(Console.ReadLine());
            numberInput = (numberInput & ~1); 
            Console.WriteLine($"Het getal is nu {numberInput}.");
            numberInput = numberInput << 3;
            Console.WriteLine($"Het getal is nu {numberInput}");
            Console.WriteLine("Geef een binaire string in:");
            numberInput = Convert.ToInt32(Console.ReadLine(),2);
            numberInput = (numberInput & ~1);
            Console.WriteLine($"Het getal is nu {Convert.ToString(numberInput,2)}");
            numberInput = numberInput << 3;
            Console.WriteLine($"Het getal is nu {Convert.ToString(numberInput,2)}");
            Console.ReadKey();

        }
    }
}
