﻿using System;

namespace H3_geometric_fun
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een hoek, uitgedrukt in graden:");
            int degrees = Convert.ToInt32(Console.ReadLine());
            double radians = degrees * Math.PI / 180;
            Console.WriteLine($"{degrees} graden is {radians:F2} radialen.");
            Console.WriteLine($"De sinus is {Math.Sin(radians):F2}.");
            Console.WriteLine($"De cosinus is {Math.Cos(radians):F2}.");
            Console.WriteLine($"De tangens is {Math.Tan(radians):F2}.");
            Console.ReadKey();
        }
    }
}