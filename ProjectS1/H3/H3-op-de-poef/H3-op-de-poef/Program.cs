﻿using System;

namespace H3_op_de_poef
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Geef een bedrag in:");
            int openAccount = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Openstaand bedrag bedraagt: {openAccount} euro.");
            
            Console.WriteLine("Geef een bedrag in:");
            openAccount += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Openstaand bedrag bedraagt: {openAccount} euro.");
            
            Console.WriteLine("Geef een bedrag in:");
            openAccount += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Openstaand bedrag bedraagt: {openAccount} euro.");

            Console.WriteLine("Geef een bedrag in:");
            openAccount += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Openstaand bedrag bedraagt: {openAccount} euro.");

            Console.WriteLine("Geef een bedrag in:");
            openAccount += Convert.ToInt32(Console.ReadLine());
            Console.WriteLine($"Openstaand bedrag bedraagt: {openAccount} euro.");

            Console.WriteLine("****************************");

            Console.WriteLine($"Het totaal openstaande bedrag bedraagt {openAccount} euro.");
            
            Console.WriteLine($"Dit zal {Math.Ceiling(openAccount / 10.0)} afbetalingen vragen.");
            Console.ReadKey();

        }
    }
}
