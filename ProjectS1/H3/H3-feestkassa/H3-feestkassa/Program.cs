﻿using System;

namespace H3_feestkassa
{
    class Program
    {
        static void Main(string[] args)
        {

            int mainCourse = 20;
            int childsCourse = 10;
            int iceCream = 3;
            int beverages = 2;
            int toPay = 0;
            Console.WriteLine("Mosselen met frietjes?");
            toPay += Convert.ToInt32(Console.ReadLine()) * mainCourse;
            Console.WriteLine("Koninginnenhapjes?");
            toPay += Convert.ToInt32(Console.ReadLine()) * childsCourse;
            Console.WriteLine("Ijsjes?");
            toPay += Convert.ToInt32(Console.ReadLine()) * iceCream;
            Console.WriteLine("Dranken?");
            toPay += Convert.ToInt32(Console.ReadLine()) * beverages;
            Console.WriteLine($"Het totaal te betalen bedrag bedraagt {toPay}");
            Console.ReadKey();
        }
    }
}
