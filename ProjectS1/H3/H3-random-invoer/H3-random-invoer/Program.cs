﻿using System;

namespace H3_random_invoer
{
    class Program
    {
        static void Main(string[] args)
        {
            Random randomNumberPrint = new Random();

            int randomNumber;
            int sum = 0;


            Console.WriteLine("Geef een bedrag in:");
            randomNumber = randomNumberPrint.Next(1, 51);
            sum += randomNumber;
            Console.WriteLine($"Automatisch gegenereerd: {randomNumber}");
            Console.WriteLine($"Openstaand bedrag bedraagt: {sum} euro.");
            
            Console.WriteLine("Geef een bedrag in:");
            randomNumber = randomNumberPrint.Next(1, 51);
            sum += randomNumber;
            Console.WriteLine($"Automatisch gegenereerd: {randomNumber}");
            Console.WriteLine($"Openstaand bedrag bedraagt: {sum} euro.");

            Console.WriteLine("Geef een bedrag in:");
            randomNumber = randomNumberPrint.Next(1, 51);
            sum += randomNumber;
            Console.WriteLine($"Automatisch gegenereerd: {randomNumber}");
            Console.WriteLine($"Openstaand bedrag bedraagt: {sum} euro.");
            
            Console.WriteLine("Geef een bedrag in:");
            randomNumber = randomNumberPrint.Next(1, 51);
            sum += randomNumber;
            Console.WriteLine($"Automatisch gegenereerd: {randomNumber}");
            Console.WriteLine($"Openstaand bedrag bedraagt: {sum} euro.");

            
            Console.WriteLine("Geef een bedrag in:");
            randomNumber = randomNumberPrint.Next(1, 51);
            sum += randomNumber;
            Console.WriteLine($"Automatisch gegenereerd: {randomNumber}");
            Console.WriteLine($"Openstaand bedrag bedraagt: {sum} euro.");

            Console.WriteLine("****************************");

            Console.WriteLine($"Het totaal openstaande bedrag bedraagt {sum} euro.");

            Console.WriteLine($"Dit zal {Math.Ceiling(sum / 10.0)} afbetalingen vragen.");
            Console.ReadKey();
        }
    }
}
